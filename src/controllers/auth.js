import User from '../models/user.js'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'

export const handleRegister = async (req, res) => {
  const user = await User.create({
    username: req.body.username,
    password: req.body.password,
  })

  user.save()
  const token = jwt.sign(
    {
      id: String(user._id),
    },
    'shinigami'
  )
  res.send({
    username: user.username,
    token,
  })
}

export const handleLogin = async (req, res) => {
  const user = await User.findOne({ username: req.body.username })

  if (!user) {
    return res.status(422).send({ Message: 'User does not exist' })
  }

  const isPasswordCorrrect = bcrypt.compareSync(
    req.body.password,
    user.password
  )
  if (!isPasswordCorrrect) {
    return res.status(422).send({ Message: 'Password#$@#$@#$@#' })
  }

  const token = jwt.sign(
    {
      id: String(user._id),
    },
    'shinigami'
  )
  res.send({ token, username: user.username })
}
