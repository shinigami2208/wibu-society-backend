import post from '../models/post.js'
import Post from '../models/post.js'
import { socket } from '../socketIO/index.js'
import cl from '../utils/cloudinary.js'

export const createPost = async (req, res) => {
  try {
    const { caption, image } = req.body
    let newPost
    if (image) {
      const uploadRespone = await cl.v2.uploader.upload(image, {
        upload_preset: 'ml_default',
      })
      if (uploadRespone) {
        newPost = await Post.create({
          caption: caption,
          author: req.user._id,
          image: uploadRespone.secure_url,
        })
      } else {
        res.status(500).send({ message: 'Error Upload Image' })
      }
    } else {
      newPost = await Post.create({
        caption: caption,
        author: req.user._id,
      })
    }
    newPost.save()
    socket.emit('CREATE_POST')
    res.send(newPost)
  } catch (error) {
    res.status(500).send({ message: 'Error creating post' })
  }
}

export const getListPost = async (req, res) => {
  const posts = await Post.find().lean()
  for (let i = 0; i < posts.length; i++) {
    posts[i].created_at = posts[i]._id.getTimestamp()
  }
  console.log(posts)
  res.send(posts)
}
