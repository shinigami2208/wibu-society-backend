import { MongoClient } from 'mongodb'
import mongoose from 'mongoose'

export default async function connectDB() {
  const uri = 'mongodb://localhost:27017/wibuland'

  mongoose
    .connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
      console.log('Connected')
    })
    .catch((error) => {
      console.log(error)
    })
}
