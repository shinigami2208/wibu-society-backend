import mongoose from 'mongoose'
import bcrypt from 'bcryptjs'

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    required: true,
    set(val) {
      return bcrypt.hashSync(val, 10)
    },
  },
})

export default mongoose.model('User', userSchema)
