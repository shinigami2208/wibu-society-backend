import mongoose from 'mongoose'

const postSchema = mongoose.Schema({
  caption: {
    type: String,
  },
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  image: {
    type: String,
  },
})

export default mongoose.model('Post', postSchema)
