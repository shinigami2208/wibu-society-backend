import express from 'express'
import User from '../models/user.js'

export const userRouter = express.Router()

userRouter.get('/:id', async (req, res) => {
  const userDetail = await User.findOne({ _id: req.params.id })
  res.send(userDetail)
})
