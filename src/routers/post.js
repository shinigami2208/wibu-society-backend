import express from 'express'
import { createPost, getListPost } from '../controllers/post.js'

export const postRouter = express.Router()

postRouter.post('/create', createPost)
postRouter.get('/', getListPost)
