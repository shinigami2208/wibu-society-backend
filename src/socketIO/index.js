import { Server } from 'socket.io'
import { createServer } from 'http'
import express from 'express'

export const app = express()
export const httpServer = createServer(app)

const io = new Server(httpServer, {
  cors: {
    origin: "http://localhost:3000"
  }
})

export const socket = io.on('connection', (socket) => {
  console.log('Client connected: ', socket.id)

  socket.on('disconnect', (reason) => {
    console.log(reason)
  })

  socket.emit('POST_CREATED')
})