import jwt from 'jsonwebtoken'
import User from '../models/user.js'

export const auth = async (req, res, next) => {
  const raw = String(req.headers.authorization).split(' ')[1]
  // const token = req.headers.authorization
  const { id } = jwt.verify(raw, "shinigami")
  req.user = await User.findById({_id: id})
  next()
}
