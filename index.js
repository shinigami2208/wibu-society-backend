import express from 'express'
import cors from 'cors'
import connectDB from './src/configs/dbConnect.js'
import authRouter from './src/routers/auth.js'
import { auth } from './src/middlewares/auth.js'
import { postRouter } from './src/routers/post.js'
import { userRouter } from './src/routers/user.js'
import { app, httpServer } from './src/socketIO/index.js'

app.use(express.static('public'))
app.use(express.json({ limit: '50mb' }))
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

app.use(cors())

app.use('/api/auth', authRouter)
app.use('/api/post', auth, postRouter)
app.use('/api/users', auth, userRouter)

connectDB().catch(console.error)

httpServer.listen(8000, () => {
  console.log('Server listening on 8000')
})
